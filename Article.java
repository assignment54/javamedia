
import java.util.Date;
import java.util.List;

public class Article {
    private String id;
    private String author_id; //primary key of author to find out which article belongs to which author
    private List<String> media_urls;
    private String subject;
    private String text_content;
    private List<String> tags; // for SEO and better search options for user e.g. sports, war, politics etc.
    private Date created_on;
    private Date updated_on;
}