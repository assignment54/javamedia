import java.util.Date;
import java.util.List;

class Author {
    private String id; //primay key of author
    private String first_name;
    private String last_name;
    private String username;
    private String password;
    private Date dob;
    private String profile_pic_url;
    private String description;
    private List<String> interest_topics;
    private Date created_on;
    private Date updated_on;
}